deploy: generate-dhparam npm-build docker-build docker-push deploy-production

generate-dhparam:
	if [ ! -f "dhparam.pem" ]; then openssl dhparam -out dhparam.pem 2048; fi

npm-build:
	docker-compose run --rm -u node node npm run build

docker-build:
	docker build --pull --file=app/docker/production/nginx.docker --tag servicepush/site:${APP_IMAGE_NAME}-nginx .
	docker build --pull --file=app/docker/production/php-fpm.docker --tag servicepush/site:${APP_IMAGE_NAME}-php-fpm app
	docker build --pull --file=app/docker/production/php-cli.docker --tag servicepush/site:${APP_IMAGE_NAME}-php-cli app
	docker build --pull --file=app/docker/production/supervisor.docker --tag servicepush/site:${APP_IMAGE_NAME}-supervisor app
	docker build --pull --file=app/docker/production/mysql.docker --tag servicepush/site:${APP_IMAGE_NAME}-mysql app

docker-push:
	docker push servicepush/site:${APP_IMAGE_NAME}-nginx
	docker push servicepush/site:${APP_IMAGE_NAME}-php-fpm
	docker push servicepush/site:${APP_IMAGE_NAME}-php-cli
	docker push servicepush/site:${APP_IMAGE_NAME}-supervisor
	docker push servicepush/site:${APP_IMAGE_NAME}-mysql

deploy-production:
	ssh web@${APP_DOMAIN} 'cd site && rm -rf docker-compose.yml .env'
	scp .env web@${APP_DOMAIN}:site/.env
	scp docker-compose-production.yml web@${APP_DOMAIN}:site/docker-compose.yml
	scp app/storage/app/js/subscribe.js web@${APP_DOMAIN}:site/storage/app/js/subscribe.js
	scp app/storage/app/js/sw.zip web@${APP_DOMAIN}:site/storage/app/js/sw.zip
#	cat ~/account | ssh web@${APP_DOMAIN} 'docker login --username servicepush --password-stdin'
	ssh web@${APP_DOMAIN} 'cd site && docker-compose pull'
	ssh web@${APP_DOMAIN} 'cd site && docker-compose up --build -d'
	ssh web@${APP_DOMAIN} 'cd site && docker-compose run --rm -u www-data php-cli php artisan config:cache'
	ssh web@${APP_DOMAIN} 'cd site && docker-compose run --rm -u www-data php-cli php artisan migrate --force'
#	ssh web@${APP_DOMAIN} 'docker logout'
