<?php

Auth::routes();

Route::post('/subscribe', 'SubscriberController@store')->name('subscribe.store');
Route::post('/tasks/delivered', 'TaskController@delivered')->name('tasks.delivered');
Route::post('/tasks/passed', 'TaskController@passed')->name('tasks.passed');

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'SiteController@index')->name('sites.index');
    Route::post('/sites', 'SiteController@store')->name('sites.store');
    Route::get('/sites/create', 'SiteController@create')->name('sites.create');
    Route::put('/sites/{site}/update', 'SiteController@update')->name('sites.update');
    Route::get('/sites/{site}/show', 'SiteController@show')->name('sites.show');
    Route::delete('/sites/{site}', 'SiteController@destroy')->name('sites.destroy');
    Route::get('/sites/{site}/setting', 'SiteSettingController@index')->name('sites.setting');
    Route::get('/sites/setting/sw-file', 'SiteSettingController@downloadSwFile')->name('sites.sw-file');
    Route::get('/sites/{site}/stats', 'SiteController@stats')->name('sites.stats');

    Route::resources(['tasks' => 'TaskController']);
    Route::post('/tasks/count', 'TaskController@count')->name('tasks.count');
});
