<?php

return [
    'subject' => env('PUSH_SUBJECT'), // can be a mailto: or your website address
    'publicKey' => env('PUSH_PUBLIC_KEY'), // (recommended) uncompressed public key P-256 encoded in Base64-URL
    'privateKey' => env('PUSH_PRIVATE_KEY'), // (recommended) in fact the secret multiplier of the private key encoded in Base64-URL
    //'pemFile' => storage_path(env('PUSH_PEM_FILE')), // if you have a PEM file and can link to it on your filesystem
];
