'use strict'

const {src, dest, parallel, watch, series} = require('gulp')
const packageJSON = require('./package.json')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')
const zip = require('gulp-zip')
const replace = require('gulp-replace')
const siteUrl = packageJSON.siteUrl

const paths = {
    build: 'storage/app/js',
    devBuild: 'public',
    source: {
        swJs: 'src/sw.js',
        subscribeJs: 'src/subscribe.js',
    },
}

function devSubscribeJs() {
    return src(paths.source.subscribeJs)
        .pipe(babel({
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-class-properties'],
        }))
        .pipe(dest(paths.devBuild))
}

function devSwJs() {
    return src(paths.source.swJs)
        .pipe(babel({
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-class-properties'],
        }))
        .pipe(dest(paths.devBuild))
}

function subscribeJs() {
    return src(paths.source.subscribeJs)
        .pipe(babel({
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-class-properties'],
        }))
        .pipe(replace('http://localhost', siteUrl))
        .pipe(uglify())
        .pipe(dest(paths.build))
}

function swJs() {
    return src(paths.source.swJs)
        .pipe(babel({
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-class-properties'],
        }))
        .pipe(replace('http://localhost', siteUrl))
        .pipe(uglify())
        .pipe(zip('sw.zip'))
        .pipe(dest(paths.build))
}

function watchJs() {
    watch(paths.source.subscribeJs, devSubscribeJs)
    watch(paths.source.swJs, devSwJs)
}

exports.build = parallel(subscribeJs, swJs)

exports.default = parallel(
    series(devSubscribeJs, devSwJs),
    //watchJs,
)