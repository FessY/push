<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('site_id');
            $table->unsignedInteger('browser_id');
            $table->unsignedInteger('os_id');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('language_id');
            $table->mediumText('endpoint');
            $table->string('public_key');
            $table->string('auth_token');
            $table->string('content_encoding');
            $table->string('ip');
            $table->string('status');
            $table->timestamp('deny_at')->nullable();
            $table->timestamps();

            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->foreign('browser_id')->references('id')->on('browsers')->onDelete('cascade');
            $table->foreign('os_id')->references('id')->on('os')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribers');
    }
}
