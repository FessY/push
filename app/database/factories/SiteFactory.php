<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entity\Site;
use Faker\Generator as Faker;

$factory->define(Site::class, function (Faker $faker) {
    return [
        'domain' => $faker->domainName,
        'subscribe_count' => $faker->numberBetween(1, 100),
        'subscribe_denying' => $faker->numberBetween(1, 100),
        'status' => $faker->randomElement(array_keys(Site::statusList())),
    ];
});
