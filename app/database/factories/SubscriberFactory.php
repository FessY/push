<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entity\Site;
use App\Entity\Subscriber;
use Faker\Generator as Faker;

$factory->define(Subscriber::class, function (Faker $faker) {
    return [
        'site_id' => function () {
            return factory(Site::class)->create()->id;
        },
        'browser_id' => $faker->numberBetween(1, 3),
        'os_id' => $faker->numberBetween(1, 3),
        'country_id' => $faker->numberBetween(1, 3),
        'custom_data_id' => null,
        'endpoint' => $faker->text,
        'publicKey' => $faker->text,
        'authToken' => $faker->text,
        'contentEncoding' => $faker->text,
        'browser_language' => $faker->randomElement(['ru', 'en']),
        'ip' => $faker->ipv4,
        'status' => $faker->randomElement(array_keys(Subscriber::statusList())),
    ];
});
