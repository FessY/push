@if ($paginator->hasPages())
    <section class="section">
        <nav class="pagination" role="navigation" aria-label="pagination">
            <a href="{{ $paginator->previousPageUrl() }}" class="pagination-previous"
               @if ($paginator->onFirstPage()) disabled @endif>@lang('pagination.previous')</a>
            <a href="{{ $paginator->nextPageUrl() }}" class="pagination-next"
               @if (!$paginator->hasMorePages()) disabled @endif>@lang('pagination.next')</a>

            <ul class="pagination-list">

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li>
                                    <a class="pagination-link is-current">{{ $page }}</a>
                                </li>
                            @else
                                <li><a class="pagination-link" href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </ul>
        </nav>
    </section>
@endif
