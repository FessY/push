@extends('layouts.app')

@section('title', $task->title)

@section('content')
    <section class="hero">
        <div class="hero-body" style="padding-left: 0;">
            <div class="columns" style="align-items: center;">
                <div class="column is-narrow">
                    <h1 class="title">
                        {{ $task->title }}
                    </h1>
                </div>
                <div class="column">
                    <p>Отправлено: {{ $task->created_at->format('d.m.Y H:i') }}</p>
                </div>
            </div>
        </div>
    </section>

    <div class="columns">
        <div class="column is-narrow">
            <img src="{{ asset('storage/' . $task->image) }}" alt="{{ $task->title }}" width="128">
        </div>

        <div class="column">
            <div class="level">
                <div class="level-item has-text-centered is-justify-content-start">
                    <div>
                        <p class="heading">Количество</p>
                        <p class="title">{{ $task->total_count }}</p>
                    </div>
                </div>
                <div class="level-item has-text-centered is-justify-content-start">
                    <div>
                        <p class="heading">Отправлено</p>
                        <p class="title">{{ $task->sent_count }}</p>
                    </div>
                </div>
                <div class="level-item has-text-centered is-justify-content-start">
                    <div>
                        <p class="heading">Доставлено</p>
                        <p class="title">{{ $task->delivered_count }}</p>
                    </div>
                </div>
                <div class="level-item has-text-centered is-justify-content-start">
                    <div>
                        <p class="heading">Переходов</p>
                        <p class="title">{{ $task->passed_count }}</p>
                    </div>
                </div>
                <div class="level-item has-text-centered is-justify-content-start">
                    <div>
                        <p class="heading">Отписок</p>
                        <p class="title">{{ $task->deny_count }}</p>
                    </div>
                </div>
            </div>

            <table class="table">
                <tbody>
                <tr>
                    <td>Список получателей</td>
                    <td>{{ $task->site ? $task->site->domain : 'Все подписчики' }}</td>
                </tr>
                <tr>
                    <td>Заголовок</td>
                    <td>{{ $task->title }}</td>
                </tr>
                <tr>
                    <td>Текст</td>
                    <td>{{ $task->text }}</td>
                </tr>
                <tr>
                    <td>Ссылка</td>
                    <td>{{ $task->url }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection