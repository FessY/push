@extends('layouts.app')

@section('title', 'Мои рассылки')

@section('content')
    <section class="hero">
        <div class="hero-body" style="padding-left: 0;">
            <div class="columns">
                <div class="column is-narrow">
                    <h1 class="title">
                        Мои рассылки
                    </h1>
                </div>
                <div class="column">
                    <a href="{{ route('tasks.create') }}" class="button is-primary">Добавить рассылку</a>
                </div>
            </div>
        </div>
    </section>

    <section style="margin-bottom: 2rem;">
        <div class="box">
            <form action="?" method="GET">
                <div class="columns">
                    <div class="column is-four-fifths">
                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded has-icons-left">
                                        <input class="input" name="title" type="text" placeholder="Заголовок"
                                               value="{{ request('title') }}">
                                        <span class="icon is-small is-left">
          <i class="fas fa-align-justify"></i>
        </span>
                                    </p>
                                </div>
                                <div class="field is-narrow">
                                    <input type="hidden" name="sort"
                                           value="{{ request('sort') === 'asc' || empty(request('sort')) ? 'desc' : 'asc' }}">
                                    <button class="button is-info" onclick="">
                                        @if(request('sort') === 'desc')
                                            <i class="fas fa-arrow-up"></i>
                                        @else
                                            <i class="fas fa-arrow-down"></i>
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="buttons">
                            <button type="submit" class="button is-success">Поиск</button>
                            <a href="?" class="button">Очистить</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

    @foreach($tasks as $task)
        <div class="box">
            <div class="media">
                <figure class="media-left">
                    <p class="image is-128x128">
                        <img src="{{ $task->image ? asset('storage/' . $task->image) : url('/images/128.png') }}"
                             alt="">
                    </p>
                </figure>
                <div class="media-content">
                    <div class="content columns">
                        <div class="column">
                            <a href="{{ route('tasks.show', $task) }}">{{ $task->title }}</a>
                            <p>Отправлено {{ $task->created_at->format('d.m.Y H:i') }}</p>
                            <p>{{ $task->url }}</p>
                        </div>
                        <div class="level column" style="display: flex;">
                            <div class="level-item has-text-centered">
                                <div>
                                    <p class="heading">Количество</p>
                                    <p class="title">{{ $task->total_count }}</p>
                                </div>
                            </div>
                            <div class="level-item has-text-centered">
                                <div>
                                    <p class="heading">Отправлено</p>
                                    <p class="title">{{ $task->sent_count }}</p>
                                </div>
                            </div>
                            <div class="level-item has-text-centered">
                                <div>
                                    <p class="heading">Доставлено</p>
                                    <p class="title">{{ $task->delivered_count }}</p>
                                </div>
                            </div>
                            <div class="level-item has-text-centered">
                                <div>
                                    <p class="heading">Переходов</p>
                                    <p class="title">{{ $task->passed_count }}</p>
                                </div>
                            </div>
                            <div class="level-item has-text-centered">
                                <div>
                                    <p class="heading">Отписок</p>
                                    <p class="title">{{ $task->deny_count }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-right">
                    <form action="{{ route('tasks.destroy', $task) }}" method="POST"
                          onsubmit="return confirm('Вы уверены?')">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="delete"></button>
                    </form>
                </div>
            </div>
        </div>
    @endforeach

    {{ $tasks->links('vendor.pagination.default') }}
@endsection
