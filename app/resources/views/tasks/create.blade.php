@extends('layouts.app')

@section('title', 'Создание рассылки')

@section('content')
    <section class="hero">
        <div class="hero-body" style="padding-left: 0;">
            <h1 class="title">
                Создание рассылки
            </h1>
        </div>
    </section>

    @if(count($errors) > 0)
        <div class="notification is-danger">
            <button class="delete"></button>
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif

    <form action="{{ route('tasks.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="columns">

            <div class="column">
                <segmentation :sites='{{ $sites }}'
                              :browsers='@json($browsers)'
                              :countries='@json($countries)'
                              :languages='@json($languages)'
                              :os='@json($os)'
                              :variables='@json($variables)'
                        {{--                              :oldFields='@json(old())'--}}
                ></segmentation>

                <div class="field">
                    <label class="label" for="title">Заголовок</label>
                    <div class="control">
                        <input type="text" name="title" class="input @error('title') is-danger @enderror" id="title"
                               value="{{ old('title') }}"
                               placeholder="До 50 символов">
                    </div>
                    @error('title')
                    <div class="help is-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="field">
                    <label class="label" for="text">Текст уведомления</label>
                    <div class="control">
                        <textarea name="text" id="text" class="textarea @error('text') is-danger @enderror"
                                  rows="3"
                                  placeholder="До 125 символов">{{ old('text') }}</textarea>
                    </div>
                    @error('text')
                    <div class="help is-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="field">
                    <label class="label" for="url">Ссылка на уведомлении</label>
                    <div class="control">
                        <input id="url" type="text" name="url" class="input @error('url') is-danger @enderror"
                               value="{{ old('url') }}"
                               placeholder="https://example.com">
                    </div>
                    @error('url')
                    <div class="help is-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="field">
                    <label class="label" for="image">Картинка 128x128</label>
                    <div class="file has-name is-boxed @error('image') is-danger @enderror">
                        <label class="file-label">
                            <input id="image" class="file-input" type="file" name="image">
                            <span class="file-cta">
                                <span class="file-icon">
                                  <i class="fas fa-cloud-upload-alt"></i>
                                </span>
                                <span class="file-label">
                                    Картинка 128x128
                                </span>
                              </span>
                            <span class="file-name">Выберите картинку</span>
                        </label>
                    </div>
                </div>

            </div>

            <div class="column">

                <div class="column">
                    <p class="title is-4">Дополнительные опции</p>

                    <div class="field">
                        <label class="label" for="utm_campaign">UTM Campaign</label>
                        <div class="control">
                            <input id="utm_campaign" type="text" name="utm_campaign"
                                   class="input @error('utm_campaign') is-danger @enderror"
                                   value="{{ old('utm_campaign', '{utm_campaign}') }}">
                        </div>
                        @error('utm_campaign')
                        <div class="help is-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="field">
                        <label class="label" for="big-image">Картинка 400x200</label>
                        <div class="file has-name is-boxed @error('big_image') is-danger @enderror">
                            <label class="file-label">
                                <input id="big-image" class="file-input" type="file" name="big_image">
                                <span class="file-cta">
                                <span class="file-icon">
                                  <i class="fas fa-cloud-upload-alt"></i>
                                </span>
                                <span class="file-label">
                                  Картинка 400x200
                                </span>
                              </span>
                                <span class="file-name">Выберите картинку</span>
                            </label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="columns">
            <div class="column">
                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-primary is-large">Отправить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
