<nav class="navbar is-light" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="/">
            {{ config('app.name') }}
        </a>

        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false"
           data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu">
        @if(auth()->check())
            <div class="navbar-start">
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="{{ route('tasks.index') }}">
                        Мои рассылки
                    </a>

                    <div class="navbar-dropdown">
                        <a class="navbar-item" href="{{ route('tasks.create') }}">
                            Создать рассылку
                        </a>
                    </div>
                </div>

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="{{ route('sites.index') }}">
                        Сайты
                    </a>

                    <div class="navbar-dropdown">
                        <a class="navbar-item" href="{{ route('sites.create') }}">
                            Добавить сайт
                        </a>
                    </div>
                </div>
            </div>
        @endif

        <div class="navbar-end">
            <div class="navbar-item">
                <div class="buttons">
                    @guest
                        <a class="button is-primary" href="{{ route('login') }}">
                            {{ __('Login') }}
                        </a>
                    @else
                        <a class="button is-light" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</nav>