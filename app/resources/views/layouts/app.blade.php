<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ url('/style.css') }}">
</head>
<body>
<div id="app">
    @include('layouts.partials.menu')

    <main class="main container">
        @include('layouts.partials.flash')
        @yield('content')
    </main>
</div>

<script src="{{ url('/js/index.js') }}"></script>
@yield('script')
</body>
</html>
