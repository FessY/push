@extends('layouts.app')

@section('title', 'Настройки сайта')

@section('content')
    <section class="hero">
        <div class="hero-body" style="padding-left: 0;">
            <h1 class="title">
                Настройки сайта
            </h1>
        </div>
    </section>

    <div class="tabs is-boxed">
        <ul>
            <li class="is-active" data-target="#site-form"><a>Общие настройки</a></li>
            <li data-target="#site-integration"><a>Интеграция с сайтом</a></li>
            <li data-target="#site-variables"><a>Интеграция переменных</a></li>
        </ul>
    </div>

    <div id="site-form">
        <form action="{{ route('sites.update', $site) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="field">
                <label class="label" for="title">Домен ({{ $site->domain }})</label>
                <div class="control">
                    <input type="text" name="domain" class="input @error('domain') is-danger @enderror" id="title"
                           value="{{ old('domain') }}"
                           placeholder="https://domain.com">
                </div>
                @error('domain')
                <div class="help is-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="field">
                <label class="label" for="image">Картинка 128x128</label>
                <div class="file has-name is-boxed @error('image') is-danger @enderror">
                    <label class="file-label">
                        <input id="image" class="file-input" type="file" name="image">
                        <span class="file-cta">
                                <span class="file-icon">
                                  <i class="fas fa-cloud-upload-alt"></i>
                                </span>
                                <span class="file-label">
                                  Выберите картинку
                                </span>
                              </span>
                        <span class="file-name">
                      Выберите картинку
                    </span>
                        @error('image')
                        <div class="help is-danger">{{ $message }}</div>
                        @enderror
                    </label>
                </div>
            </div>

            <div class="field">
                <div class="control">
                    <button type="submit" class="button is-primary is-medium">Сохранить</button>
                </div>
            </div>
        </form>
    </div>

    <div id="site-integration" class="is-hidden">
        <div class="box">
            <p class="content">Скопируйте и вставьте код на ваш сайт, перед закрывающим тегом <span
                        class="tag is-info">{{ '</head>' }}</span></p>
            <pre class="content">{{ '<script charset="UTF-8" src="//'.\App\Helpers\ClearHttpForDomain::clear(asset('storage/' . $site->script)).'" async></script>' }}</pre>
            <p class="content">Чтобы была подписка по кнопке вставьте следующий код:</p>
            <pre class="content">{{ '<button id="subscribe">Подписаться</button>' }}</pre>
            <p class="content">Также распакуйте и положите в корень основной скрипт уведомлений</p>
            <a href="{{ route('sites.sw-file') }}" class="button is-info">Скачать zip</a>
        </div>
    </div>

    <div id="site-variables" class="is-hidden">
        <div class="box">
            <p>Вставьте в любое место сайта следующий код:</p>
            <pre style="margin-top: 1em;">{{ '<input type="hidden" name="'}}<span
                        class="tag is-link">name</span>{{'" value="' }}<span
                        class="tag is-link">peremennaya</span>{{ '" data-variable>' }}</pre>
        </div>
    </div>
@endsection