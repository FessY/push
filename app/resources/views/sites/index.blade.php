@extends('layouts.app')

@section('content')
    @env('local')
        <button id="subscribe">Подписаться</button>
        <script src="{{ url('/subscribe.js') }}" async></script>
    @endenv

    <section class="hero">
        <div class="hero-body" style="padding-left: 0;">
            <div class="columns">
                <div class="column is-narrow">
                    <h1 class="title">
                        Мои сайты
                    </h1>
                </div>
                <div class="column">
                    <a href="{{ route('sites.create') }}" class="button is-primary">Добавить сайт</a>
                </div>
            </div>
        </div>
    </section>

    @foreach($sites as $site)
        <div class="box">
            <div class="media">
                <figure class="media-left">
                    <p class="image is-128x128">
                        <img src="{{ $site->image ? asset('storage/' . $site->image) : url('/images/128.png') }}">
                    </p>
                </figure>
                <div class="media-content">
                    <div class="content">
                        <a href="{{ route('sites.show', $site) }}">{{ $site->domain }}</a>
                        <p>Подписчиков: <strong>{{ $site->subscribers_count }}</strong></p>
                    </div>
                </div>
                <div class="media-right">
                    <form action="{{ route('sites.destroy', $site) }}" method="POST" onsubmit="return confirm('Вы уверены?')">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="delete"></button>
                    </form>
                </div>
            </div>
        </div>
    @endforeach

    {{ $sites->links('vendor.pagination.default') }}
@endsection
