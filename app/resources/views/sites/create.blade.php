@extends('layouts.app')

@section('title', 'Добавление сайта')

@section('content')
    <section class="hero">
        <div class="hero-body" style="padding-left: 0;">
            <h1 class="title">
                Добавление сайта
            </h1>
        </div>
    </section>

    <form action="{{ route('sites.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="field">
            <label class="label" for="title">Домен</label>
            <div class="control">
                <input type="text" name="domain" class="input @error('domain') is-danger @enderror" id="title"
                       value="{{ old('domain') }}"
                       placeholder="https://domain.com">
            </div>
            @error('domain')
            <div class="help is-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="field">
            <label class="label" for="image">Картинка 128x128</label>
            <div class="file has-name is-boxed @error('image') is-danger @enderror">
                <label class="file-label">
                    <input id="image" class="file-input" type="file" name="image">
                    <span class="file-cta">
                                <span class="file-icon">
                                  <i class="fas fa-cloud-upload-alt"></i>
                                </span>
                                <span class="file-label">
                                  Картинка 128x128
                                </span>
                              </span>
                    <span class="file-name">
                      Выберите картинку
                    </span>
                    @error('image')
                    <div class="help is-danger">{{ $message }}</div>
                    @enderror
                </label>
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-primary is-large">Следующий шаг</button>
            </div>
        </div>
    </form>
@endsection

@section('script')
    <script>

    </script>
@endsection