@extends('layouts.app')

@section('title', $site->domain)

@section('content')
    <section class="hero">
        <div class="hero-body" style="padding-left: 0;">
            <div class="columns">
                <div class="column is-narrow">
                    <h1 class="title">
                        {{ $site->domain }}
                    </h1>
                </div>
                <div class="column">
                    <a href="{{ route('sites.setting', $site) }}" class="button is-primary">Настройка сайта</a>
                </div>
            </div>
        </div>
    </section>

    <chart :site='@json($site)'></chart>

    <section class="section">
        <div class="box">
            <nav class="level">
                <div class="level-item has-text-centered">
                    <div>
                        <p class="heading">Активные подписчики</p>
                        <p class="title">{{ $site->subscribers_count }}</p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <p class="heading">Подписчики за сегодня</p>
                        <p class="title">{{ $site->subscribers_today }}</p>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div>
                        <p class="heading">Количество отписок</p>
                        <p class="title">{{ $site->subscribers_denying }}</p>
                    </div>
                </div>
            </nav>
        </div>
    </section>
@endsection