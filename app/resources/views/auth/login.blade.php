@extends('layouts.app')

@section('content')
    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <div class="box">
                        <p class="subtitle is-4 has-text-grey">{{ __('Login') }}</p><br>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="field">
                                <p class="control has-icons-left has-icons-right">

                                    <input class="input is-medium @error('email') is-danger @enderror"
                                           type="email"
                                           name="email"
                                           placeholder="{{ __('E-Mail Address') }}"
                                           value="{{ old('email') }}"
                                           required
                                           autocomplete="email"
                                           autofocus>
                                    <span class="icon is-medium is-left">
                    <i class="fas fa-envelope"></i>
                  </span>
                                </p>
                                @error('email')
                                <div class="help is-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="field">
                                <p class="control has-icons-left">

                                    <input class="input is-medium @error('password') is-danger @enderror"
                                           name="password"
                                           type="password"
                                           placeholder="{{ __('Password') }}">

                                    <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                  </span>
                                </p>
                                @error('password')
                                <div class="help is-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            {{-- <div class="field">
                                 <label class="checkbox">
                                     <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                     <label for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                 </label>
                             </div>--}}

                            <button type="submit"
                                    class="button is-block is-info is-medium is-fullwidth">{{ __('Login') }}</button>
                        </form>
                    </div>
                    {{--<p class="has-text-grey">
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                        @endif
                    </p>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
