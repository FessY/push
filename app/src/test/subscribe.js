(function () {
    const server = 'http://localhost/subscribe'
    const applicationServerKey = 'BA8p1Mka7sCiE-2qtok7ofH9YdgghfZU705jtjoADKT3FCQjJb8WtLu2BGahqG279RteCHRLCYwGZ2OxhYJUBuc'

    class Subscription {

        detectOs() {
            const ua = navigator.userAgent

            return -1 !== ua.indexOf('Windows')
                ? 'Windows' : -1 !== ua.indexOf('Android')
                    ? 'Android' : -1 !== ua.indexOf('Linux')
                        ? 'Linux' : -1 !== ua.indexOf('iPhone')
                            ? 'iOS' : -1 !== ua.indexOf('Mac')
                                ? 'Mac OS' : -1 !== ua.indexOf('FreeBSD')
                                    ? 'FreeBSD' : ''
        }

        getBrowserLanguage() {
            return navigator.language.substring(0, 2)
        }

        detectBrowser() {
            const ua = navigator.userAgent
            let tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || []
                return 'IE ' + (tem[1] || '')
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\b(OPR|Edge?)\/(\d+)/)
                if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera').replace('Edg ', 'Edge ')
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?']
            if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1])
            return {
                name: M[0],
                version: M[1],
            }
        }

        getUserVariables() {
            const customFields = document.querySelectorAll('input[data-variable]')
            const obj = {}

            for (let i = 0; i < customFields.length; i++)
                switch (customFields[i].type) {
                    case 'text':
                    case 'hidden':
                        obj[customFields[i].name] = customFields[i].value
                        break
                    case 'checkbox':
                        obj[customFields[i].name] = customFields[i].checked ? 1 : 0
                        break
                    case 'radio':
                        customFields[i].checked && (obj[customFields[i].name] = customFields[i].value)
                }

            return obj
        }

        urlBase64ToUint8Array(base64String) {
            const padding = '='.repeat((4 - (base64String.length % 4)) % 4)
            const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/')

            const rawData = window.atob(base64)
            const outputArray = new Uint8Array(rawData.length)

            for (let i = 0; i < rawData.length; ++i) {
                outputArray[i] = rawData.charCodeAt(i)
            }
            return outputArray
        }

        notify() {
            if (!('Notification' in window)) {
                alert('Ваш браузер не поддерживает HTML Notifications, его необходимо обновить.')
                return
            }

            Notification.requestPermission().then(subscribe.subscribe)
        }

        subscribe() {
            if (!('serviceWorker' in navigator)) {
                console.error('Service Worker is not supported')
            }
            subscribe.pushSubscribe()
        }

        pushSubscribe() {
            return navigator.serviceWorker.register('sw.js')
                .then(
                    () => navigator.serviceWorker.ready,
                    e => console.error('[x] Service worker registration failed', e),
                )
                .then(serviceWorkerRegistration => serviceWorkerRegistration.pushManager.subscribe({
                        userVisibleOnly: true,
                        applicationServerKey: subscribe.urlBase64ToUint8Array(applicationServerKey),
                    }),
                )
                .then(subscription => {
                    const key = subscription.getKey('p256dh')
                    const token = subscription.getKey('auth')
                    const contentEncoding = (PushManager.supportedContentEncodings || ['aesgcm'])[0]

                    fetch(server, {
                        method: 'post',
                        headers: {'Content-Type': 'application/json'},
                        body: JSON.stringify({
                            browser: subscribe.detectBrowser(),
                            os: subscribe.detectOs(),
                            lang: subscribe.getBrowserLanguage(),
                            variables: subscribe.getUserVariables(),
                            url: window.location.href,
                            endpoint: subscription.endpoint,
                            publicKey: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
                            authToken: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null,
                            contentEncoding,
                        }),
                    }).catch(function (err) {
                        console.error('[x] Error', err)
                    })
                })
                .catch(e => {
                    if (Notification.permission === 'denied') {
                        console.warn('Notifications are denied by the user.')
                    } else {
                        console.error('Impossible to subscribe to push notifications', e)
                    }
                })
        }

        start() {
            switch (Notification.permission) {
                case 'granted':
                    return
                case 'denied':
                    return
                default:
                    const button = document.querySelector('#subscribe')

                    if (button) {
                        button.addEventListener('click', () => {
                            this.notify()
                        })
                    } else {
                        this.subscribe()
                    }
            }
        }
    }

    const subscribe = new Subscription()

    window.addEventListener('load', () => {
        subscribe.start()
    })
})()