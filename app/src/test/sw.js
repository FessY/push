'use strict'

self.addEventListener('install', function (event) {
    event.waitUntil(self.skipWaiting())
})

self.addEventListener('push', function (event) {
    if (!event.data) {
        return
    }

    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return
    }

    const message = JSON.parse(event.data.text())

    const sendNotification = function (message) {
        self.registration.showNotification(message.title, {
            body: message.body,
            icon: message.icon,
            image: message.image,
            data: {redirectUrl: message.url},
            tag: message.taskId,
            requireInteraction: true,
        })

        return fetch('http://localhost/tasks/delivered', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                subscriberId: message.subscriberId,
                taskId: message.taskId,
            }),
        })
    }

    event.waitUntil(sendNotification(message))
})

self.addEventListener('notificationclick', function (event) {
    const url = event.notification.data.redirectUrl
    event.notification.close()
    event.waitUntil(clients.matchAll({type: 'window'})
        .then(function (clientList) {
            for (let i = 0; i < clientList.length; i++) {
                const client = clientList[i]

                if (client.url === '/' && 'focus' in client) {
                    return client.focus()
                }

                if (clients.openWindow) {
                    return clients.openWindow(url)
                }
            }
        }).then(function () {
            return fetch('http://localhost/tasks/passed', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    taskId: event.notification.tag,
                }),
            })
        }),
    )
})
