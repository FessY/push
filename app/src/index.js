import Vue from 'vue'
import axios from 'axios'
import './styles/style.scss'
import '@fortawesome/fontawesome-free/js/all'

window.axios = axios
let token = document.head.querySelector('meta[name="csrf-token"]')
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
} else {
    console.error('CSRF token not found')
}

Vue.component('segmentation', require('./components/Segmentation.vue').default)
Vue.component('chart', require('./components/Chart.vue').default)

new Vue({
    el: '#app',
})

document.addEventListener('DOMContentLoaded', () => {
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0)
    if ($navbarBurgers.length > 0) {
        $navbarBurgers.forEach(el => {
            el.addEventListener('click', () => {
                const target = el.dataset.target
                const $target = document.getElementById(target)
                el.classList.toggle('is-active')
                $target.classList.toggle('is-active')
            })
        })
    }

    const tabItems = Array.prototype.slice.call(document.querySelectorAll('.tabs > ul > li'), 0)
    if (tabItems.length > 0) {
        tabItems.forEach(function (el) {
            el.addEventListener('click', () => {
                const target = document.querySelector(el.dataset.target)
                const currentActive = el.parentElement.querySelector('.is-active')
                const currentTarget = document.querySelector(currentActive.dataset.target)
                currentTarget.classList.toggle('is-hidden')
                currentActive.classList.toggle('is-active')
                el.classList.toggle('is-active')
                target.classList.toggle('is-hidden')
            })
        })
    }

    const fileInput = Array.prototype.slice.call(document.querySelectorAll('.file.has-name > .file-label > .file-input'), 0)
    if (fileInput.length > 0) {
        fileInput.forEach(function (el) {
            el.addEventListener('change', () => {
                const fileName = el.parentElement.querySelector('.file-name')
                fileName.innerHTML = el.files[0].name
            })
        })
    }

    const notificationItems = Array.prototype.slice.call(document.querySelectorAll('.notification > .delete'), 0);
    if (notificationItems.length > 0) {
        notificationItems.forEach(function(el) {
            el.addEventListener('click', () => {
                const notification = el.parentElement;
                notification.classList.add('is-hidden');
            });
        });
    }
})