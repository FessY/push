<?php

namespace App\Providers;

use App\Services\Push\Push;
use App\Services\Push\PushSender;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class PushServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(PushSender::class, function (Application $app) {
            $config = $app->make('config')->get('push');

            $auth = [
                //'GCM' => 'MY_GCM_API_KEY', // deprecated and optional, it's here only for compatibility reasons
                'VAPID' => $config,
            ];

            return new Push($auth);
        });
    }
}
