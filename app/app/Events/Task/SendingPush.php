<?php

namespace App\Events\Task;

use App\Entity\Task;
use App\UseCases\Tasks\DTO\Filter;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class SendingPush
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Task
     */
    public $task;
    /**
     * @var Filter
     */
    public $filter;


    /**
     * Create a new event instance.
     *
     * @param Task $task
     * @param Filter $filter
     */
    public function __construct(Task $task, Filter $filter)
    {
        $this->task = $task;
        $this->filter = $filter;
    }
}
