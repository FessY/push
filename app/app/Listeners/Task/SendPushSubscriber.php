<?php

namespace App\Listeners\Task;

use App\Events\Task\SendingPush;
use App\Jobs\Subscriber\Push;
use App\QueryBuilders\SubscribeQuery;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPushSubscriber implements ShouldQueue
{
    private const SUBSCRIBER_COUNT = 100;

    /**
     * Handle the event.
     *
     * @param SendingPush $event
     * @return void
     */
    public function handle(SendingPush $event)
    {
        $task = $event->task;
        $filter = $event->filter;

        SubscribeQuery::getQuery($filter)->chunk(self::SUBSCRIBER_COUNT, function ($subscribers) use ($task) {
            foreach ($subscribers as $subscriber) {
                Push::dispatch($subscriber, $task)->delay(now()->addSeconds(3));
                $task->incrTotalCount();
            }
        });
    }
}
