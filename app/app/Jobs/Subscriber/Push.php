<?php

namespace App\Jobs\Subscriber;

use App\Entity\Subscriber;
use App\Entity\Task;
use App\Services\Push\PushSender;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Minishlink\WebPush\Subscription;

class Push implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Subscriber
     */
    private $subscriber;
    /**
     * @var Task
     */
    private $task;

    /**
     * Create a new job instance.
     *
     * @param Subscriber $subscriber
     * @param Task $task
     */
    public function __construct(Subscriber $subscriber, Task $task)
    {
        $this->subscriber = $subscriber;
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @param PushSender $push
     * @return void
     * @throws \ErrorException
     */
    public function handle(PushSender $push)
    {
        $data = [
            'title' => $this->task->title,
            'body' => $this->task->text,
            'icon' => asset('storage/' . $this->task->image),
            'url' => $this->task->url,
            'taskId' => $this->task->id,
            'subscriberId' => $this->subscriber->id,
        ];

        if ($this->subscriber->hasChrome()) {
            $data['image'] = $this->task->big_image ? asset('storage/' . $this->task->big_image) : '';
        }

        $push->sendNotification(
            new Subscription(
                $this->subscriber->endpoint,
                $this->subscriber->public_key,
                $this->subscriber->auth_token,
                $this->subscriber->content_encoding
            ),
            json_encode($data)
        );

        foreach ($push->flush() as $report) {
            $endpoint = $report->getRequest()->getUri()->__toString();

            $this->task->incrSentCount();

            if ($report->isSuccess()) {
                Log::info("[v] Message sent successfully for subscription {$endpoint}.");
            } else {
                $this->task->incrDenyCount();
                $this->subscriber->unactive();
                Log::error("[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}");
            }
        }

    }
}
