<?php


namespace App\QueryBuilders;


use App\Entity\Subscriber;
use App\UseCases\Tasks\DTO\Filter;
use Illuminate\Database\Eloquent\Builder;

class SubscribeQuery
{
    public static function getQuery(Filter $filter): Builder
    {
        return Subscriber::active()
            ->when($filter->getVariableId(), function (Builder $q, $variableId) {
                $q->leftJoin('subscriber_variable', 'subscribers.id', '=', 'subscriber_variable.subscriber_id')
                    ->where('subscriber_variable.variable_id', $variableId);
            })->when($filter->isNotEmpty(), function (Builder $q) use ($filter) {
                $q->where($filter->getData());
            });
    }
}