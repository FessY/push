<?php

namespace App\Console\Commands;

use App\Entity\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reg:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('What is the name?');
        $email = $this->ask('What is the email?');
        $password = $this->secret('What is the password?');

        /** @var User $user */
        $user = User::where('email', $email)->first();

        if (!is_null($user)) {
            $this->error("Email - {$email} is exists.");
            return false;
        }

        try {
            User::create(['name' => $name, 'email' => $email, 'password' => bcrypt($password)]);
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
            return false;
        }

        $this->info('User is successfully register.');
        return true;
    }
}
