<?php

namespace App\Console\Commands;

use App\Entity\Task;
use Illuminate\Console\Command;
use Redis;

class SyncRedisTaskStatistics extends Command
{
    private const TASK_COUNT = 100;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:task-statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync redis task statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Task::chunk(self::TASK_COUNT, function ($tasks) {
            /** @var Task $task */
            foreach ($tasks as $task) {
                $task->syncStatistic(
                    Redis::get(Task::REDIS_TOTAL . $task->id) ?? 0,
                    Redis::get(Task::REDIS_SENT . $task->id) ?? 0,
                    Redis::get(Task::REDIS_DELIVERED . $task->id) ?? 0,
                    Redis::get(Task::REDIS_PASSED . $task->id) ?? 0,
                    Redis::get(Task::REDIS_DENY . $task->id) ?? 0
                );
            }
        });

        $this->info('Sync is successfully.');

        return true;
    }
}
