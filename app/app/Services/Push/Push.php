<?php

namespace App\Services\Push;

use Minishlink\WebPush\WebPush;

class Push extends WebPush implements PushSender
{
    public function __construct(array $auth)
    {
        parent::__construct($auth);
    }
}