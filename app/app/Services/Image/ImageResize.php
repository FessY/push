<?php

namespace App\Services\Image;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class ImageResize
{
    public function resizeToPublic(UploadedFile $image, string $path, int $width = 128, int $height = 128)
    {
        $fullPath = storage_path("app/public/{$path}");
        $filename = $image->hashName();
        $imageResize = Image::make($image->getRealPath());
        $imageResize->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });

        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }

        $imageResize->save("{$fullPath}/{$filename}");

        return "{$path}/{$filename}";
    }
}