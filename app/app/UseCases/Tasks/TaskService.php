<?php

namespace App\UseCases\Tasks;

use App\Entity\Site;
use App\Entity\Task;
use App\Entity\Task\Statistic;
use App\Events\Task\SendingPush;
use App\Services\Image\ImageResize;
use App\UseCases\Tasks\DTO\Filter;
use App\UseCases\Tasks\DTO\Utm;
use Illuminate\Http\UploadedFile;

class TaskService
{
    /**
     * @var ImageResize
     */
    private $imageResize;

    /**
     * TaskService constructor.
     * @param ImageResize $imageResize
     */
    public function __construct(ImageResize $imageResize)
    {
        $this->imageResize = $imageResize;
    }

    public function create(
        ?int $siteId,
        string $title,
        string $text,
        string $url,
        ?UploadedFile $image,
        ?UploadedFile $bigImage,
        Utm $utm,
        Filter $filter
    ): Task
    {
        /** @var Site $site */
        $site = $siteId ? Site::findOrFail($siteId) : null;
        $imageFilePath = $image ? $this->imageResize->resizeToPublic($image, 'tasks/images') : optional($site)->image;
        $bigImageFilePath = $bigImage ? $this->imageResize->resizeToPublic($bigImage, 'tasks/images', 400, 200) : null;

        $task = Task::new(
            $site,
            $title,
            $text,
            $url,
            $imageFilePath,
            $bigImageFilePath
        );

        $task->saveOrFail();

        Statistic::new($task->id);

        $task->addUtmCampaignByUrl($utm);

        $task->saveOrFail();

        event(new SendingPush($task, $filter));

        return $task;
    }
}