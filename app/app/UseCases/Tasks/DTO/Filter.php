<?php

namespace App\UseCases\Tasks\DTO;

class Filter
{
    /**
     * @var int
     */
    private $siteId;
    /**
     * @var int|null
     */
    private $browserId;
    /**
     * @var int|null
     */
    private $countryId;
    /**
     * @var int|null
     */
    private $languageId;
    /**
     * @var int|null
     */
    private $osId;
    /**
     * @var int|null
     */
    private $variableId;

    public function __construct(
        ?int $siteId,
        ?int $browserId,
        ?int $countryId,
        ?int $languageId,
        ?int $osId,
        ?int $variableId
    )
    {
        $this->siteId = $siteId;
        $this->browserId = $browserId;
        $this->countryId = $countryId;
        $this->languageId = $languageId;
        $this->osId = $osId;
        $this->variableId = $variableId;
    }

    public function getData(): array
    {
        if($this->siteId) {
            $data['site_id'] = $this->siteId;
        }

        if ($this->browserId) {
            $data['browser_id'] = $this->browserId;
        }

        if ($this->countryId) {
            $data['country_id'] = $this->countryId;
        }

        if ($this->languageId) {
            $data['language_id'] = $this->languageId;
        }

        if ($this->osId) {
            $data['os_id'] = $this->osId;
        }

        return $data ?? [];
    }

    public function isNotEmpty(): bool
    {
        return count($this->getData()) > 0;
    }

    public function getVariableId()
    {
        return $this->variableId;
    }
}