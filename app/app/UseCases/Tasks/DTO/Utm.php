<?php

namespace App\UseCases\Tasks\DTO;

class Utm
{
    /**
     * @var string|null
     */
    public $utmCampaign;

    public function __construct(?string $utmCampaign)
    {
        $this->utmCampaign = $utmCampaign;
    }
}