<?php

namespace App\UseCases\Subscribers;

use App\Entity\Browser;
use App\Entity\Country;
use App\Entity\Lang;
use App\Entity\Os;
use App\Entity\Site;
use App\Entity\Subscriber;
use App\Entity\Variable;
use App\Helpers\ClearHttpForDomain;
use App\UseCases\Subscribers\DTO\Info;
use App\UseCases\Subscribers\DTO\PushEndpoint;
use DB;

class SubscribeService
{
    public function create(string $url, PushEndpoint $pushEndpoint, Info $info)
    {
        /** @var Site $site */
        $site = Site::where('domain', ClearHttpForDomain::clear($url))->firstOrFail();
        /** @var Os $os */
        $os = Os::where('name', $info->os)->first();
        /** @var Lang $language */
        $language = Lang::where('name', $info->language)->first();
        /** @var Browser $browser */
        $browser = Browser::where('name', $info->browserName)->first();
        /** @var Country $country */
        $country = Country::name($info->country)->first();

        DB::transaction(function () use (
            $country,
            $site,
            $browser,
            $language,
            $os,
            $pushEndpoint,
            $info
        ) {
            if (!$os) {
                $os = Os::new($info->os);
            }

            if (!$language) {
                $language = Lang::new($info->language);
            }

            if (!$browser) {
                $browser = Browser::new($info->browserName);
            }

            if (!$country) {
                $country = Country::new($info->country);
            }

            $variableIds = [];
            foreach ($info->variables as $value) {
                if (!$variable = Variable::where('name', $value)->first()) {
                    $variable = Variable::new($value);
                }
                $variableIds[] = $variable->id;
            }

            $subscriber = Subscriber::new(
                $pushEndpoint->endpoint,
                $pushEndpoint->publicKey,
                $pushEndpoint->authToken,
                $pushEndpoint->contentEncoding,
                $info->ip
            );

            $subscriber->site()->associate($site);
            $subscriber->os()->associate($os);
            $subscriber->language()->associate($language);
            $subscriber->browser()->associate($browser);
            $subscriber->country()->associate($country);

            $subscriber->saveOrFail();

            $subscriber->variables()->attach($variableIds);
        });
    }
}