<?php

namespace App\UseCases\Subscribers\DTO;

class PushEndpoint
{
    /**
     * @var string
     */
    public $endpoint;
    /**
     * @var string
     */
    public $publicKey;
    /**
     * @var string
     */
    public $authToken;
    /**
     * @var string
     */
    public $contentEncoding;

    public function __construct(string $endpoint, string $publicKey, string $authToken, string $contentEncoding)
    {
        $this->endpoint = $endpoint;
        $this->publicKey = $publicKey;
        $this->authToken = $authToken;
        $this->contentEncoding = $contentEncoding;
    }
}