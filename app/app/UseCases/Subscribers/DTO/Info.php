<?php

namespace App\UseCases\Subscribers\DTO;

class Info
{
    /**
     * @var string
     */
    public $ip;
    /**
     * @var string
     */
    public $os;
    /**
     * @var string
     */
    public $language;
    /**
     * @var string
     */
    public $browserName;
    /**
     * @var string
     */
    public $browserVersion;
    /**
     * @var array
     */
    public $variables;
    /**
     * @var string
     */
    public $country;

    public function __construct(
        string $ip,
        string $os,
        string $language,
        array $browser,
        string $country,
        array $variables
    )
    {
        $this->ip = $ip;
        $this->os = $os;
        $this->language = $language;
        $this->browserName = $browser['name'];
        $this->browserVersion = $browser['version'];
        $this->country = base64_decode($country);
        $this->variables = $variables;
    }
}