<?php

namespace App\UseCases\Sites;

use App\Entity\Site;
use App\Helpers\ClearHttpForDomain;
use App\Services\Image\ImageResize;
use App\UseCases\Sites\DTO\Stats;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\UploadedFile;
use Storage;
use Str;

class SiteService
{
    public const SCRIPT_PATCH = 'js/subscribe.js';
    public const IMG_WIDTH = 128;
    public const IMG_HEIGHT = 128;
    /**
     * @var ImageResize
     */
    private $imageResize;

    public function __construct(ImageResize $imageResize)
    {
        $this->imageResize = $imageResize;
    }

    public function create(string $url, ?UploadedFile $image)
    {
        $scriptPatch = 'sites/js/' . str_replace('-', '', Str::uuid()->toString()) . '.js';
        Storage::copy(self::SCRIPT_PATCH, 'public/' . $scriptPatch);
        $imageFilePath = $image ? $this->imageResize->resizeToPublic($image, 'sites/images') : null;
        $site = Site::new(ClearHttpForDomain::clear($url), $scriptPatch, $imageFilePath);

        $site->saveOrFail();
    }

    public function stats(int $siteId, ?string $date): Stats
    {
        $site = Site::findOrFail($siteId);
        $originDate = $date ? Carbon::parse($date) : Carbon::today();
        $month = $originDate->month;
        $year = $originDate->year;
        $daysInMonth = $originDate->daysInMonth;

        $totalSubscribers = $this->getSubscribers($site, $month, $year, 'created_at');
        $unactiveSubscribers = $this->getSubscribers($site, $month, $year, 'deny_at');

        $days = $subscriberActive = $subscriberCompleted = $subscriberTotal = [];
        for ($day = 1; $day <= $daysInMonth; $day++) {
            $date = Carbon::create($year, $month, $day)->toDateString();
            $days[] = $day;
            $totalSubscriber = $totalSubscribers[$date] ?? 0;
            $unactiveSubscriber = $unactiveSubscribers[$date] ?? 0;
            $subscriberTotal[] = $totalSubscriber;
            $subscriberCompleted[] = $unactiveSubscriber;
            $subscriberActive[] = $totalSubscriber - $unactiveSubscriber;
        }

        $months = [];
        $period = CarbonPeriod::create($site->created_at->toDateString(), Carbon::today());
        /** @var Carbon $d */
        foreach ($period as $d) {
            $months[$d->format('Y-m')] = $d->monthName;
        }

        return new Stats($originDate, $months, $days, $subscriberActive, $subscriberCompleted, $subscriberTotal);
    }

    private function getSubscribers(Site $site, string $month, string $year, string $field): array
    {
        return $site->subscribers()
            ->selectRaw("COUNT(id) as count, DATE({$field}) as date")
            ->whereMonth($field, $month)
            ->whereYear($field, $year)
            ->groupBy('date')
            ->get()
            ->flatMap(function ($item) use ($field) {
                return [$item->date => $item->count];
            })->toArray();
    }
}