<?php


namespace App\UseCases\Sites\DTO;


use Carbon\Carbon;

class Stats
{
    /**
     * @var Carbon
     */
    public $originDate;
    /**
     * @var array
     */
    public $months;
    /**
     * @var array
     */
    public $days;
    /**
     * @var array
     */
    public $subscriberActive;
    /**
     * @var array
     */
    public $subscriberCompleted;
    /**
     * @var array
     */
    public $subscriberTotal;

    /**
     * Stats constructor.
     * @param Carbon $originDate
     * @param array $months
     * @param array $days
     * @param array $subscriberActive
     * @param array $subscriberCompleted
     * @param array $subscriberTotal
     */
    public function __construct(
        Carbon $originDate,
        array $months,
        array $days,
        array $subscriberActive,
        array $subscriberCompleted,
        array $subscriberTotal
    )
    {
        $this->originDate = $originDate;
        $this->months = $months;
        $this->days = $days;
        $this->subscriberActive = $subscriberActive;
        $this->subscriberCompleted = $subscriberCompleted;
        $this->subscriberTotal = $subscriberTotal;
    }
}