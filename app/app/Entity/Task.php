<?php

namespace App\Entity;

use App\Entity\Task\Statistic;
use App\UseCases\Tasks\DTO\Utm;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Task
 *
 * @property int $id
 * @property int $site_id
 * @property string $title
 * @property string $text
 * @property string $url
 * @property string $image
 * @property string|null $big_image
 * @property string $status
 * @property Site $site
 * @property Statistic $statistic
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class Task extends Model
{
    public const ACTIVE_STATUS = 'active';
    public const COMPLETED_STATUS = 'completed';

    public const REDIS_TOTAL = 'tasks:total:';
    public const REDIS_SENT = 'tasks:sent:';
    public const REDIS_DENY = 'tasks:deny:';
    public const REDIS_DELIVERED = 'tasks:delivered:';
    public const REDIS_PASSED = 'tasks:passed:';

    public const UTM_CAMPAIGN_TEMPLATE = '{utm_campaign}';

    protected $table = 'tasks';

    protected $guarded = [];

    protected $with = ['statistic'];

    /**
     * @param Site $site
     * @param string $title
     * @param string $text
     * @param string $url
     * @param string $imageFilePath
     * @param string|null $bigImageFilePath
     * @return Task|Model
     */
    public static function new(?Site $site, string $title, string $text, string $url, string $imageFilePath, ?string $bigImageFilePath): self
    {
        return self::make([
            'site_id' => $site ? $site->id : null,
            'title' => $title,
            'text' => $text,
            'url' => $url,
            'image' => $imageFilePath,
            'big_image' => $bigImageFilePath,
            'status' => self::ACTIVE_STATUS
        ]);
    }

    public function addUtmCampaignByUrl(Utm $utm)
    {
        if (!$utm->utmCampaign) {
            return;
        }

        $utmCampaign = $utm->utmCampaign === self::UTM_CAMPAIGN_TEMPLATE ? $this->id : $utm->utmCampaign;

        $this->url = "{$this->url}?utm_campaign={$utmCampaign}&utm_medium=push";
    }

    public function getSubscriberCount()
    {
        return $this->site->subscribers->count();
    }

    /**
     * @return Statistic|\Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(Statistic::class, 'task_id');
    }

    /**
     * @return Site|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class, 'site_id');
    }

    public function incrTotalCount()
    {
        $this->statistic->incr(self::REDIS_TOTAL . $this->id);
    }

    public function incrPassedCount()
    {
        $this->statistic->incr(self::REDIS_PASSED . $this->id);
    }

    public function incrDeliveredCount()
    {
        $this->statistic->incr(self::REDIS_DELIVERED . $this->id);
    }

    public function incrSentCount()
    {
        $this->statistic->incr(self::REDIS_SENT . $this->id);
    }

    public function incrDenyCount()
    {
        $this->statistic->incr(self::REDIS_DENY . $this->id);
    }

    public function getRedisKeys()
    {
        return [
            self::REDIS_TOTAL . $this->id,
            self::REDIS_DELIVERED . $this->id,
            self::REDIS_PASSED . $this->id,
            self::REDIS_SENT . $this->id,
            self::REDIS_DENY . $this->id,
        ];
    }

    public function getTotalCountAttribute()
    {
        return $this->statistic->getTotalCount(self::REDIS_TOTAL . $this->id);
    }

    public function getDeliveredCountAttribute()
    {
        return $this->statistic->getDeliveredCount(self::REDIS_DELIVERED . $this->id);
    }

    public function getPassedCountAttribute()
    {
        return $this->statistic->getPassedCount(self::REDIS_PASSED . $this->id);
    }

    public function getSentCountAttribute()
    {
        return $this->statistic->getSentCount(self::REDIS_SENT . $this->id);
    }

    public function getDenyCountAttribute()
    {
        return $this->statistic->getDenyCount(self::REDIS_DENY . $this->id);
    }

    public function syncStatistic(
        int $totalCount,
        int $sentCount,
        int $deliveredCount,
        int $passedCount,
        int $denyCount
    )
    {
        $this->statistic->sync($totalCount, $sentCount, $deliveredCount, $passedCount, $denyCount);
    }
}
