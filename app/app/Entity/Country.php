<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Country
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Country name($value)
 * @mixin \Eloquent
 */
class Country extends Model
{
    public const UNDEFINED = 'Неизвестно';

    protected $table = 'countries';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public static function new(string $name)
    {
        return static::create(['name' => !empty($name) ? $name : self::UNDEFINED]);
    }

    public function scopeName(Builder $query, string $name)
    {
        return $query->where('name', !empty($name) ? $name : self::UNDEFINED);
    }
}
