<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Subscriber
 *
 * @property int $id
 * @property int $site_id
 * @property int $browser_id
 * @property int $os_id
 * @property int $country_id
 * @property int $language_id
 * @property int|null $custom_data_id
 * @property string $endpoint
 * @property string $public_key
 * @property string $auth_token
 * @property string $content_encoding
 * @property string $ip
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deny_at
 * @property-read \App\Entity\Browser $browser
 * @property-read \App\Entity\Country $country
 * @property-read \App\Entity\Lang $language
 * @property-read \App\Entity\Os $os
 * @property-read \App\Entity\Site $site
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereAuthToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereBrowserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereContentEncoding($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereCustomDataId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereEndpoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereOsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber wherePublicKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereSiteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subscriber whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Subscriber extends Model
{
    public const ACTIVE_STATUS = 'active';
    public const COMPLETED_STATUS = 'completed';

    public static function statusList(): array
    {
        return [
            self::ACTIVE_STATUS => 'Активный',
            self::COMPLETED_STATUS => 'Завершенный',
        ];
    }

    protected $guarded = [];

    protected $dates = ['deny_at'];

    /**
     * @param string $endpoint
     * @param string $publicKey
     * @param string $authToken
     * @param string $contentEncoding
     * @param string $ip
     * @return Subscriber|Model
     */
    public static function new(
        string $endpoint,
        string $publicKey,
        string $authToken,
        string $contentEncoding,
        string $ip
    ): Subscriber
    {
        return static::make([
            'endpoint' => $endpoint,
            'public_key' => $publicKey,
            'auth_token' => $authToken,
            'content_encoding' => $contentEncoding,
            'ip' => $ip,
            'status' => self::ACTIVE_STATUS,
        ]);
    }

    public function site()
    {
        return $this->belongsTo(Site::class, 'site_id');
    }

    public function os()
    {
        return $this->belongsTo(Os::class, 'os_id');
    }

    public function browser()
    {
        return $this->belongsTo(Browser::class, 'browser_id');
    }

    public function language()
    {
        return $this->belongsTo(Lang::class, 'language_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function variables()
    {
        return $this->belongsToMany(Variable::class, 'subscriber_variable', 'subscriber_id', 'variable_id');
    }

    public function hasChrome(): bool
    {
        return $this->browser->name === 'Chrome';
    }

    public function unactive()
    {
        $this->deny_at = Carbon::now();
        $this->status = self::COMPLETED_STATUS;
        $this->saveOrFail();
    }

    public function isActive(): bool
    {
        return $this->status === self::ACTIVE_STATUS;
    }

    public function isCompleted(): bool
    {
        return $this->status === self::COMPLETED_STATUS;
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', self::ACTIVE_STATUS);
    }
}
