<?php

namespace App\Entity\Task;

use App\Entity\Task;
use Illuminate\Database\Eloquent\Model;
use Redis;

/**
 * Class Statistic
 * @property int $id
 * @property int $task_id
 * @property int $total_count
 * @property int $sent_count
 * @property int $delivered_count
 * @property int $passed_count
 * @property int $deny_count
 * @property Task $task
 * @mixin \Eloquent
 */
class Statistic extends Model
{
    protected $table = 'statistics';

    protected $guarded = [];

    public static function new(int $taskId): Statistic
    {
        return self::create(['task_id' => $taskId]);
    }

    public function incr(string $name)
    {
        Redis::incr($name);
    }

    /**
     * @param string $name
     * @return int
     */
    public function getTotalCount(string $name): int
    {
        return $this->getTotal($name, $this->total_count);
    }

    /**
     * @param string $name
     * @return int
     */
    public function getPassedCount(string $name): int
    {
        return $this->getTotal($name, $this->passed_count);
    }

    /**
     * @param string $name
     * @return int
     */
    public function getDeliveredCount(string $name): int
    {
        return $this->getTotal($name, $this->delivered_count);
    }

    /**
     * @param string $name
     * @return int
     */
    public function getSentCount(string $name): int
    {
        return $this->getTotal($name, $this->sent_count);
    }

    /**
     * @param string $name
     * @return int
     */
    public function getDenyCount(string $name): int
    {
        return $this->getTotal($name, $this->deny_count);
    }

    private function getTotal($name, $tableName)
    {
        $count = Redis::get($name);

        if ($count > 0) {
            return $count;
        }

        return $tableName;
    }

    public function sync(
        int $totalCount,
        int $sentCount,
        int $deliveredCount,
        int $passedCount,
        int $denyCount
    )
    {
        $this->update([
            'total_count' => $totalCount,
            'sent_count' => $sentCount,
            'delivered_count' => $deliveredCount,
            'passed_count' => $passedCount,
            'deny_count' => $denyCount,
        ]);
    }
}