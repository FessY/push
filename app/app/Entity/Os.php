<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Os
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os whereName($value)
 * @mixin \Eloquent
 */
class Os extends Model
{
    protected $table = 'os';

    public $timestamps = false;

    public static function new(string $os)
    {
        return static::create(['name' => $os]);
    }

    protected $fillable = [
        'name',
    ];
}
