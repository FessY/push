<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Site
 *
 * @property int $id
 * @property string $domain
 * @property int $subscribe_count
 * @property int $subscribe_denying
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Browser[] $browsers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Lang[] $languages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Os[] $os
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Subscriber[] $subscribers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site whereSubscribeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site whereSubscribeDenying($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Site withSubscriberCount()
 * @mixin \Eloquent
 */
class Site extends Model
{
    protected $table = 'sites';

    public const ACTIVE_STATUS = 'active';
    public const COMPLETED_STATUS = 'completed';

    public static function statusList(): array
    {
        return [
            self::ACTIVE_STATUS => 'Активный',
            self::COMPLETED_STATUS => 'Завершенный',
        ];
    }

    protected $guarded = [];

    /**
     * @param string $domain
     * @param string $script
     * @param string $image
     * @return Site|Model
     */
    public static function new(string $domain, string $script, ?string $image): Site
    {
        return self::make([
            'domain' => $domain,
            'image' => $image,
            'script' => $script,
            'status' => self::ACTIVE_STATUS,
        ]);
    }

    public function subscribers()
    {
        return $this->hasMany(Subscriber::class, 'site_id');
    }

    public function scopeWithSubscriberCount(Builder $builder)
    {
        return $builder->withCount(['subscribers' => function (Builder $query) {
            $query->active();
        }]);
    }
}
