<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Browser
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Browser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Browser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Browser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Browser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Browser whereName($value)
 * @mixin \Eloquent
 */
class Browser extends Model
{
    protected $table = 'browsers';

    public $timestamps = false;

    public static function new(string $name)
    {
        return static::create(['name' => $name]);
    }

    protected $fillable = [
        'name',
    ];
}
