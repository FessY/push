<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Lang
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Lang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Lang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Lang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Lang whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Lang whereName($value)
 * @mixin \Eloquent
 */
class Lang extends Model
{
    protected $table = 'languages';

    public $timestamps = false;

    public static function new(string $language)
    {
        return static::create(['name' => $language]);
    }

    protected $fillable = [
        'name',
    ];
}
