<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Variable
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Os whereName($value)
 * @mixin \Eloquent
 */
class Variable extends Model
{
    protected $table = 'variables';

    public $timestamps = false;

    public static function new(string $variable)
    {
        return static::create(['name' => $variable]);
    }

    protected $fillable = [
        'name',
    ];
}
