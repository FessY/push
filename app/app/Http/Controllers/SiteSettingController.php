<?php

namespace App\Http\Controllers;

use App\Entity\Site;
use Response;
use Storage;

class SiteSettingController extends Controller
{
    public function index(Site $site)
    {
        return view('sites.settings.index', compact('site'));
    }

    public function downloadSwFile()
    {
        return response()->download(storage_path('app/js/sw.zip'));
    }
}
