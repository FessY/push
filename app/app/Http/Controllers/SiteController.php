<?php

namespace App\Http\Controllers;

use App\Entity\Site;
use App\Entity\Subscriber;
use App\Helpers\ClearHttpForDomain;
use App\UseCases\Sites\SiteService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Storage;

class SiteController extends Controller
{
    /**
     * @var SiteService
     */
    private $service;

    public function __construct(SiteService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $sites = Site::withSubscriberCount()->paginate(10);
        return view('sites.index', compact('sites'));
    }

    public function create()
    {
        return view('sites.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'domain' => 'required|url',
            'image' => 'nullable|image'
        ]);

        $this->service->create($request['domain'], $request->file('image'));

        return redirect(route('sites.index'))->with('success', 'Site is created');
    }

    public function show(int $id)
    {
        $site = Site::withCount([
            'subscribers' => function (Builder $query) {
                $query->where('status', Subscriber::ACTIVE_STATUS);
            },
            'subscribers as subscribers_today' => function (Builder $query) {
                $query->whereDate('created_at', Carbon::today());
            },
            'subscribers as subscribers_denying' => function (Builder $query) {
                $query->where('status', Subscriber::COMPLETED_STATUS);
            }
        ])->findOrFail($id);

        return view('sites.show', compact('site'));
    }

    public function stats(int $siteId, Request $request)
    {
        $stats = $this->service->stats($siteId, $request->get('date'));

        return response()->json([
            'selectedMonth' => $stats->originDate->format('Y-m'),
            'months' => $stats->months,
            'chart' => [
                'labels' => $stats->days,
                'datasets' => [
                    [
                        'label' => 'Активное количество подписчиков',
                        'backgroundColor' => '#1e90ff',
                        'data' => $stats->subscriberActive,
                    ],
                    [
                        'label' => 'Количество отписок',
                        'backgroundColor' => '#cccccc',
                        'data' => $stats->subscriberCompleted,
                    ],
                    [
                        'label' => 'Количество подписчиков',
                        'backgroundColor' => '#f87979',
                        'data' => $stats->subscriberTotal,
                        'type' => 'line',
                        'fill' => false,
                        'showLine' => false,
                    ],
                ]
            ]
        ]);
    }

    public function update(Request $request, Site $site)
    {
        $this->validate($request, [
            'domain' => 'required|url',
            'image' => 'nullable|image|dimensions:min_width=128,min_height=128'
        ]);

        $site->update([
            'domain' => ClearHttpForDomain::clear($request['domain']),
            'image' => $request->file('image') ? $request->file('image')->store('sites/images', 'public') : null,
        ]);

        return redirect(route('sites.setting', $site))->with('success', 'Site is updated');
    }

    public function destroy(Site $site)
    {
        Storage::disk('public')->delete([$site->image, $site->script]);
        $site->delete();
        return redirect(route('sites.index'))->with('success', 'Site is deleted');
    }
}
