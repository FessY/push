<?php

namespace App\Http\Controllers;

use App\Entity\Browser;
use App\Entity\Country;
use App\Entity\Lang;
use App\Entity\Os;
use App\Entity\Site;
use App\Entity\Task;
use App\Entity\Variable;
use App\QueryBuilders\SubscribeQuery;
use App\UseCases\Tasks\DTO\Filter;
use App\UseCases\Tasks\DTO\Utm;
use App\UseCases\Tasks\TaskService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Redis;
use Storage;

class TaskController extends Controller
{
    /**
     * @var TaskService
     */
    private $service;

    public function __construct(TaskService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $query = Task::query();
        $sort = $request->get('sort') ?? 'asc';

        if (!empty($value = $request->get('title'))) {
            $query->where('title', 'like', '%' . $value . '%');
        }

        $tasks = $query->orderBy('created_at', $sort)->paginate(10);

        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sites = Site::withSubscriberCount()->get();

        $browsers = Browser::pluck('name', 'id');
        $countries = Country::pluck('name', 'id');
        $os = Os::pluck('name', 'id');
        $languages = Lang::pluck('name', 'id');
        $variables = Variable::pluck('name', 'id');

        return view(
            'tasks.create',
            compact('sites', 'browsers', 'countries', 'os', 'languages', 'variables')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'site_id' => 'nullable|integer',
            'title' => 'required',
            'text' => 'required',
            'url' => 'required',
            'image' => [
                Rule::requiredIf(empty($request['site_id'])),
                'image',
            ],
            'big_image' => 'nullable|image',
            'subscriber_count' => 'required|integer|min:1',
        ]);

        $task = $this->service->create(
            $request['site_id'],
            $request['title'],
            $request['text'],
            $request['url'],
            $request->file('image'),
            $request->file('big_image'),
            new Utm($request['utm_campaign']),
            new Filter(
                $request['site_id'],
                $request->input('filter_browser_id'),
                $request->input('filter_country_id'),
                $request->input('filter_language_id'),
                $request->input('filter_os_id'),
                $request->input('filter_variable_id')
            )
        );

        return redirect(route('tasks.show', $task));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Entity\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Entity\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Entity\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Entity\Task $task
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Task $task)
    {
        Storage::disk('public')->delete($task->image);
        Storage::disk('public')->delete($task->big_image);
        Redis::del($task->getRedisKeys());
        $task->delete();
        return back()->with('success', 'Task is deleted');
    }

    public function count(Request $request)
    {
        $count = 0;
        $filter = new Filter(
            $request->json('site_id'),
            $request->json('browser_id'),
            $request->json('country_id'),
            $request->json('language_id'),
            $request->json('os_id'),
            $request->json('variable_id')
        );

        if ($filter->isNotEmpty()) {
            $count = SubscribeQuery::getQuery($filter)->count();
        }

        return response()->json([
            'count' => $count,
            'text' => trans_choice('segmentation.subscribers', $count)
        ]);
    }

    public function delivered(Request $request)
    {
        $task = Task::findOrFail($request->json('taskId'));
        $task->incrDeliveredCount();
        return response()->noContent();
    }

    public function passed(Request $request)
    {
        $task = Task::findOrFail($request->json('taskId'));
        $task->incrPassedCount();
        return response()->noContent();
    }
}
