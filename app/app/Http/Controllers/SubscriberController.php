<?php

namespace App\Http\Controllers;

use App\Entity\Subscriber;
use App\UseCases\Subscribers\DTO\Info;
use App\UseCases\Subscribers\DTO\PushEndpoint;
use App\UseCases\Subscribers\SubscribeService;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    /**
     * @var SubscribeService
     */
    private $service;

    public function __construct(SubscribeService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->service->create(
            $request->server('HTTP_REFERER'),
            new PushEndpoint(
                $request['endpoint'],
                $request['publicKey'],
                $request['authToken'],
                $request['contentEncoding']
            ),
            new Info(
                $request->ip(),
                $request['os'],
                $request['lang'],
                $request['browser'],
                $request->server('GEOIP_COUNTRY_NAME'),
                $request['variables']
            )
        );

        return response()->noContent();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Entity\Subscriber $subscriber
     * @return \Illuminate\Http\Response
     */
    public function show(Subscriber $subscriber)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Entity\Subscriber $subscriber
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscriber $subscriber)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Entity\Subscriber $subscriber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscriber $subscriber)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Entity\Subscriber $subscriber
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscriber $subscriber)
    {
        //
    }
}
