<?php


namespace App\Helpers;


class ClearHttpForDomain
{
    /**
     * @param string $url
     * @return string
     */
    public static function clear(string $url)
    {
        $domain = parse_url($url, PHP_URL_HOST);
        return preg_replace('/^(www\.)?/', '', $domain);
    }
}